<?php

# Aplikacja
define("APP_TITLE", "Diagnostics");

# Katalogi
define("ROOT", "/");
define("CONF_DIR", ROOT."conf/");
define("POLYGON_DIR", CONF_DIR."polygon/");
define("DEPS_DIR", ROOT."deps/");
define("INDEX_DIR", ROOT."php/");
define("SCRIPTS_DIR", ROOT."scripts/");
define("STYLE_DIR", SCRIPTS_DIR."styles/");
define("JSON_DIR", SCRIPTS_DIR."json/");
define("BOOTSTRAP_DIR", STYLE_DIR."bootstrap/");
define("SASS_DIR",STYLE_DIR."sass/");
define("JS_DIR", POLYGON_DIR."js/");
define("DEFINES_FILE", POLYGON_DIR."php/conf_defines.php");
define("WEBPACK_CONF_FILE", ROOT."webpack.config.js");

# Odwołanie do konfiguracji.
/*require(JSON_DIR);
require(JSON_DIR);*/

# Wersje
define("MIN_PHP_VERSION_REQUIRED", "7.0.0"); # Wyprodukuje ostrzezenie w konsoli
define("APP_CURRENT_VERSION", "0.01");
define("APP_CURRECT_CHANNEL", "pre-release/beta");
define("CI_VERSION", "1.06-gitlabfoss-e3");

# Komunikaty dot. konfiguracji
define("PUBLIC_VCS_CONF_FILE",__DIR__.'/vcs.json');
define("PUBLIC_CONTAINERS_CONF_FILE",__DIR__.'/containers.json');
define("REQUIREMENTS_NOT_MET", "Zaleznosci nie zostaly spelnione");
define("GENERIC_ERROR", "Błąd. Kontynuacja nie mozliwa");
define("VCS_CONFIG_FILE_ERROR","Wskazany plik konfiguracyjny VCS jest pusty, nie istnieje, albo ma złe prawa RWX");

# Debugowanie ( 0: wyłączone; 1: włączone )
define("DEBUG_MODE",0);

# Logger
define("LOGGER_INIT_ERROR", "Błąd inicjacji modułu logującego");
define("LOGGER_INIT_OK", "Moduł logujący zainicjowany prawidłowo");
define("LOG_PATH", ROOT."logs");

# SSL
define("SSL_ENABLED", true);
define("SSL_CERT_KEY_PAIR", true);
define("SSL_FUNC_FILE", INDEX_DIR."inc/functions.php");

# WebPack
define("WEBPACK_ACTIVE", 1);
define("WEBPACK_INSTALLATION_TYPE", [
    "LOCAL", "REMOTE"
]);
define("WEBPACK_SSL_MODE", 1);
define("WEBPACK_CONSTS", [
    "PATH", "FILE_TYPE",
    "FILE_ASSOC", "PATH_TYPE"
]);
define("WEBPACK_ACTIVATED", "Plugin jest juz aktywny i nie ma potrzeby reaktywacji");
define("WEBPACK_UPDATE_NEEDED", "Nowa wersja jest dostępna");


# Test wyswietlania stalych.
# Odkomentuj linijke ponizej, wpisz NAZWE (pierwsza wartosc) stalej i uruchom kod ( domyslnie Control + 5 ))
# echo JS_DIR;